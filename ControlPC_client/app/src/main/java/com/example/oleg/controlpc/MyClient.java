package com.example.oleg.controlpc;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.UnknownHostException;

/**
 * Created by Oleg on 03.01.2017.
 */

public class MyClient {
    private InetAddress _ipAddress;
    private int _port;
    private SocketAddress _address;
    Socket clientSocket;
    PrintWriter out;
    MainActivity _main;
    public MyClient(String ip_new, int port, MainActivity main)
    {
        _main = main;
        changeConfig(ip_new, port);
    }

    public void changeConfig(String ip_new, int port)
    {
        try {
            _ipAddress = InetAddress.getByName(ip_new);
        } catch (Exception e) {
            e.printStackTrace();
            _main.showMessage("IP не правильный", "Socket");
            return;
        }
        _port = port;
        _address = new InetSocketAddress(_ipAddress, _port);
    }

    public int getPort()
    {
        return _port;
    }

    public String getIpAddress()
    {
        return _ipAddress.toString().substring(1);
    }

    public void Send(String message)
    {
        try {
            clientSocket = new Socket();
            clientSocket.connect(_address, 1000);
            out = new PrintWriter(clientSocket.getOutputStream(), true);
            out.print(message);
            out.close();
            clientSocket.close();
        }
        catch (Exception e)
        {
            _main.showMessage("Не удалось отправить сообщение\n"+e.getMessage(), "Socket");
        }
        //out.close();
    }
}

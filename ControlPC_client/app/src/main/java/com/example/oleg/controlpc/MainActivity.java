package com.example.oleg.controlpc;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    Settings settings;
    MyClient client;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        setContentView(R.layout.activity_main);
        SharedPreferences sharedPref = getSharedPreferences("ControlPC", 0);
        String ip = sharedPref.getString("IP", "127.0.0.1");
        int port = sharedPref.getInt("PORT", 1234);
        client = new MyClient(ip, port, this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.main, menu);
        super.onCreateOptionsMenu(menu);
        menu.add("menu1");
        menu.add("menu2");
        menu.add("menu3");
        menu.add("menu4");
        getMenuInflater().inflate(R.menu.main, menu);
        super.onCreateOptionsMenu(menu);
        return true;
    }

    public void SettingsButton(View v)
    {
        String ip = client.getIpAddress();
        int port = client.getPort();
        Intent intent = new Intent(this, Settings.class);
        Bundle bundle = new Bundle();
        bundle.putString("IP", ip);
        bundle.putInt("PORT", port);
        intent.putExtras(bundle);
        startActivityForResult(intent, 1);
    }

    public void VolumeButtonPlus(View v)
    {
        client.Send("VolumePlus");
    }

    public void showMessage(String message, String title)
    {
        AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(this);
        dlgAlert.setMessage(message);
        dlgAlert.setTitle(title);
        dlgAlert.setPositiveButton("OK", null);
        dlgAlert.setCancelable(true);
        dlgAlert.show();
    }

    public void VolumeButtonMinus(View v)
    {
        client.Send("VolumeMinus");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null) {return;}
        if (resultCode > 0) {return;}
        String ip = data.getStringExtra("IP");
        int port = data.getIntExtra("PORT", 1234);
        client.changeConfig(ip, port);
    }
}

package com.example.oleg.controlpc;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Created by Oleg on 03.01.2017.
 */

public class Settings extends AppCompatActivity {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        port_editText = (EditText)findViewById(R.id.port_editText);
        ipAddress_editText = (EditText)findViewById(R.id.ipAddress_editText);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        String ip = bundle.getString("IP");
        int port = bundle.getInt("PORT", 1234);

        _ipAddress = ip;
        _port = port;
        port_editText.setText(String.valueOf(_port));
        ipAddress_editText.setText(_ipAddress);
    }


    public Settings()
    {

    }

    private int _port = 1234;
    private String _ipAddress = "127.0.0.1";
    EditText port_editText;
    EditText ipAddress_editText;
    MyClient _client;

    public void showMessage(String message, String title)
    {
        AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(this);
        dlgAlert.setMessage(message);
        dlgAlert.setTitle(title);
        dlgAlert.setPositiveButton("OK", null);
        dlgAlert.setCancelable(true);
        dlgAlert.show();
    }

    public void SaveButton(View v)
    {
        String port_str = port_editText.getText().toString();
        String ipAddress_str = ipAddress_editText.getText().toString();
        int port = 1234;
        try {
            port = Integer.parseInt(port_str);
        } catch (Exception e){
            e.printStackTrace();
            showMessage("Порт не правильный", "Порт");
            return;
        }

        try {
            InetAddress ip = InetAddress.getByName(ipAddress_str);
            ipAddress_str = ip.toString().substring(1);
        } catch (Exception e) {
            e.printStackTrace();
            showMessage("IP не правильный", "Socket");
            return;
        }

        SharedPreferences sharedPref = getSharedPreferences("ControlPC", 0);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("IP", ipAddress_str);
        editor.putInt("PORT", port);
        editor.apply();

        Intent intent = new Intent();
        intent.putExtra("IP", ipAddress_str);
        intent.putExtra("PORT", port);
        setResult(0, intent);
        finish();
    }

    public void CancelButton(View v)
    {
        setResult(1);
        finish();
    }


}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace ControlPC
{
    class MyServer
    {
        TcpListener server = null;
        TcpClient client = null;
         int port = -1;
         IPAddress localAddr = null;        


        public MyServer(int port, IPAddress ipAddress)
        {
            localAddr = ipAddress;
            this.port = port;
            server = new TcpListener(localAddr, port);
        }

        bool _running = false;

        public bool Running
        {
            get { return _running; }
        }

        void _start(IAsyncResult ar)
        {
            TcpListener serv = (TcpListener)ar.AsyncState;
            try
            {
                client = serv.EndAcceptTcpClient(ar);
                NetworkStream stream = client.GetStream();
                List<byte> bytes = new List<byte>();
                int byt;
                while ((byt = stream.ReadByte()) > -1)
                {
                    bytes.Add((byte)byt);
                }
                string text = Encoding.UTF8.GetString(bytes.ToArray());
                del(text);
                server.BeginAcceptTcpClient(new AsyncCallback(_start), server);
            }
            catch (ObjectDisposedException ex)
            {
                del("Ошибка в _start " + ex.Message);
            }
        }

        Form1.myDelegate del;

        public void Start(Form1.myDelegate del)
        {           
            this.del = del;
            _running = true;
            server.Start();
            server.BeginAcceptTcpClient(new AsyncCallback(_start), server);              
        }

        public void Stop()
        {
            _running = false;
            //client.Close();
            server.Stop();    
            
        }

  
    }
}

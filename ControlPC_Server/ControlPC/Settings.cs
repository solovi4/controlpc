﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace ControlPC
{
    class Settings
    {
        static string fileName = "settings.xml";
        static int _port = 1234;
        static IPAddress _ipAddress = IPAddress.Parse("127.0.0.1");
        public static int Port
        {
            get { return _port; }            
        }

        public static IPAddress IpAddress
        {
            get { return _ipAddress; }
        }

        public static void Save(int newPort, string newIpAddress)
        {
            _port = newPort;
            _ipAddress = IPAddress.Parse(newIpAddress);
            List<string> strs = new List<string>();
            strs.Add(getString(newPort.ToString(), "port"));
            strs.Add(getString(newIpAddress, "ip"));
            File.WriteAllLines(fileName, strs.ToArray());
        }

        public static void Load()
        {
            string[] strs = File.ReadAllLines(fileName);
            string port_str = getValue(strs, "port");
            string ip_str = getValue(strs, "ip");
            int port = -1;
            IPAddress ip = null;
            if (int.TryParse(port_str, out port))
            {
                _port = port;
            }

            if(IPAddress.TryParse(ip_str, out ip))
            {
                _ipAddress = ip;
            }
        }

        static string getValue(string[] strs, string tag)
        {
            string result = "";
            foreach (var str in strs)
            {
                string temp = "<"+tag+">";
                string temp2 = "</"+tag+">";
                int index = str.IndexOf(temp);
                int index2 = str.IndexOf(temp2);
                if (index > -1)
                {
                    int startIndex = index+temp.Length;
                    int len = index2 - startIndex;
                    result = str.Substring(startIndex, len);
                    break;
                }
                
            }
            return result;
        }

        static string getString(string parametr, string tag)
        {
            return "<" + tag + ">" + parametr + "</" + tag + ">";
        }
        
         
    }
}

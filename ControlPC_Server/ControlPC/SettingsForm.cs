﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows.Forms;

namespace ControlPC
{
    public partial class SettingsForm : Form
    {
        public SettingsForm()
        {
            InitializeComponent();
        }

        private void SAVE_button_Click(object sender, EventArgs e)
        {
            save();
        }

        void save()
        {
            int port = -1;
            if (int.TryParse(PORT_textBox.Text, out port) && port > -1)
            {
                string ipAddress = IPcomboBox.SelectedItem.ToString();
                Settings.Save(port, ipAddress);
            }
        }

        private void OK_button_Click(object sender, EventArgs e)
        {
            save();
            this.Close();
        }

        private void SettingsForm_Load(object sender, EventArgs e)
        {
            Settings.Load();
            PORT_textBox.Text = Settings.Port.ToString();
            
            IPAddress[] addrs = Dns.GetHostAddresses(Dns.GetHostName());            
            foreach (var addr in addrs)
            {               
                switch(addr.AddressFamily)
                {
                    case System.Net.Sockets.AddressFamily.InterNetwork: IPcomboBox.Items.Add(addr.ToString()); break;
                    //case System.Net.Sockets.AddressFamily.InterNetworkV6: IPcomboBox.Items.Add(addr.ToString()); break;
                }
            }

            IPcomboBox.SelectedItem = Settings.IpAddress.ToString();

        }
    }
}

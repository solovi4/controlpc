﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;

namespace ControlPC
{
    public partial class Form1 : Form
    {
        public delegate void myDelegate(string message);
        delegate void SetTextCallback(string text);
        MyServer server = null;
        ControlPC controlPC;
        public Form1()
        {
            InitializeComponent();
            Settings.Load();
            server = new MyServer(Settings.Port, Settings.IpAddress);
            controlPC = new ControlPC();

        }

        private void Form1_Deactivate(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                this.ShowInTaskbar = false;
                notifyIcon1.Visible = true;
            }
        }

        void myHandler(string message)
        {
            if (this.richTextBox1.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(myHandler);
                this.Invoke(d, new object[] { message });
            }
            else
            {
                controlPC.doIT(message);
                richTextBox1.Text += System.Environment.NewLine;
                richTextBox1.Text += message;
            }
        }

        private void start_button_Click(object sender, EventArgs e)
        {
            myDelegate md = myHandler;
            start_button.Enabled = false;
            stop_button.Enabled = true;
            server.Start(md);
            
        }
       
        private void stop_button_Click(object sender, EventArgs e)
        {
            start_button.Enabled = true;
            stop_button.Enabled = false;
            server.Stop();
        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SettingsForm sf = new SettingsForm();
            sf.ShowDialog();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            server.Stop();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            controlPC.doIT("VolumeMinus");
        }

        private void notifyIcon1_DoubleClick(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                this.WindowState = FormWindowState.Normal;
                this.ShowInTaskbar = true;
                notifyIcon1.Visible = false;
            } 
        }
    }
}

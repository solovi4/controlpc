﻿namespace ControlPC
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ipAddressLabel = new System.Windows.Forms.Label();
            this.PortLabel = new System.Windows.Forms.Label();
            this.PORT_textBox = new System.Windows.Forms.TextBox();
            this.Apply_button = new System.Windows.Forms.Button();
            this.OK_button = new System.Windows.Forms.Button();
            this.IPcomboBox = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // ipAddressLabel
            // 
            this.ipAddressLabel.AutoSize = true;
            this.ipAddressLabel.Location = new System.Drawing.Point(13, 13);
            this.ipAddressLabel.Name = "ipAddressLabel";
            this.ipAddressLabel.Size = new System.Drawing.Size(50, 13);
            this.ipAddressLabel.TabIndex = 0;
            this.ipAddressLabel.Text = "IP адрес";
            // 
            // PortLabel
            // 
            this.PortLabel.AutoSize = true;
            this.PortLabel.Location = new System.Drawing.Point(13, 57);
            this.PortLabel.Name = "PortLabel";
            this.PortLabel.Size = new System.Drawing.Size(32, 13);
            this.PortLabel.TabIndex = 2;
            this.PortLabel.Text = "Порт";
            // 
            // PORT_textBox
            // 
            this.PORT_textBox.Location = new System.Drawing.Point(13, 74);
            this.PORT_textBox.Name = "PORT_textBox";
            this.PORT_textBox.Size = new System.Drawing.Size(194, 20);
            this.PORT_textBox.TabIndex = 3;
            // 
            // Apply_button
            // 
            this.Apply_button.Location = new System.Drawing.Point(145, 226);
            this.Apply_button.Name = "Apply_button";
            this.Apply_button.Size = new System.Drawing.Size(75, 23);
            this.Apply_button.TabIndex = 4;
            this.Apply_button.Text = "Применить";
            this.Apply_button.UseVisualStyleBackColor = true;
            this.Apply_button.Click += new System.EventHandler(this.SAVE_button_Click);
            // 
            // OK_button
            // 
            this.OK_button.Location = new System.Drawing.Point(13, 225);
            this.OK_button.Name = "OK_button";
            this.OK_button.Size = new System.Drawing.Size(75, 23);
            this.OK_button.TabIndex = 5;
            this.OK_button.Text = "OK";
            this.OK_button.UseVisualStyleBackColor = true;
            this.OK_button.Click += new System.EventHandler(this.OK_button_Click);
            // 
            // IPcomboBox
            // 
            this.IPcomboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.IPcomboBox.FormattingEnabled = true;
            this.IPcomboBox.Location = new System.Drawing.Point(13, 30);
            this.IPcomboBox.Name = "IPcomboBox";
            this.IPcomboBox.Size = new System.Drawing.Size(194, 21);
            this.IPcomboBox.TabIndex = 6;
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(232, 261);
            this.Controls.Add(this.IPcomboBox);
            this.Controls.Add(this.OK_button);
            this.Controls.Add(this.Apply_button);
            this.Controls.Add(this.PORT_textBox);
            this.Controls.Add(this.PortLabel);
            this.Controls.Add(this.ipAddressLabel);
            this.Name = "SettingsForm";
            this.Text = "Настройки";
            this.Load += new System.EventHandler(this.SettingsForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label ipAddressLabel;
        private System.Windows.Forms.Label PortLabel;
        private System.Windows.Forms.TextBox PORT_textBox;
        private System.Windows.Forms.Button Apply_button;
        private System.Windows.Forms.Button OK_button;
        private System.Windows.Forms.ComboBox IPcomboBox;
    }
}